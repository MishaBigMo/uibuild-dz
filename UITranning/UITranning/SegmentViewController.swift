//
//  SegmentViewController.swift
//  UITranning
//
//  Created by Михаил on 22/02/2019.
//  Copyright © 2019 3M Software. All rights reserved.
//

import UIKit

class SegmentViewController: UIViewController {
    
    @IBOutlet weak var labelSegment: UILabel!
    @IBOutlet weak var segmentOutlet: UISegmentedControl!
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        
        switch segmentOutlet.selectedSegmentIndex  {
        case 0:
            labelSegment.text = "Привет!"
        case 1:
            labelSegment.text = "Hello!"
        case 2:
            labelSegment.text = "Hola!"
        default:
            labelSegment.text = "error!"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}
