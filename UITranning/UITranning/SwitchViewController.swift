//
//  SwitchViewController.swift
//  UITranning
//
//  Created by Михаил on 27/02/2019.
//  Copyright © 2019 3M Software. All rights reserved.
//

import UIKit

class SwitchViewController: UIViewController {
    
    @IBOutlet weak var backLabel: UIButton!
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var whiteLabel: UILabel!
    @IBOutlet weak var viewOutlet: UIView!
    @IBOutlet weak var switchOutlet: UISwitch!
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func switchAction(_ sender: UISwitch) {
        
        if sender.isOn == true {
            viewOutlet.backgroundColor = .red
            redLabel.textColor = .white
            whiteLabel.textColor = .white
            backLabel.titleLabel?.textColor = .white
        } else {
            viewOutlet.backgroundColor = .clear
            redLabel.textColor = .black
            whiteLabel.textColor = .black
            backLabel.titleLabel?.textColor = .black

        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
