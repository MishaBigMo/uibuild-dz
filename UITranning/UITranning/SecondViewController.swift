//
//  SecondViewController.swift
//  UITranning
//
//  Created by Михаил on 27/02/2019.
//  Copyright © 2019 3M Software. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var name: String = ""
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonOutlet: UIButton!
    @IBOutlet weak var label: UILabel!
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonAction(_ sender: UIButton) {
        name = textField.text!
        if name != "" {
            label.text = "Hello, \(name)!"
        } else {
            label.text = "Please, enter your name!"
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
