//
//  StepperViewController.swift
//  UITranning
//
//  Created by Михаил on 22/02/2019.
//  Copyright © 2019 3M Software. All rights reserved.
//

import UIKit

class StepperViewController: UIViewController {
    
    @IBOutlet weak var labelStepper: UILabel!
    @IBOutlet weak var stepperOutlet: UIStepper!
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        
        labelStepper.text = Int(sender.value).description
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}
