//
//  SliderViewController.swift
//  UITranning
//
//  Created by Михаил on 20/02/2019.
//  Copyright © 2019 3M Software. All rights reserved.
//

import UIKit

class SliderViewController: UIViewController {
    
    @IBOutlet weak var appleOutlet: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var sliderOutlet: UISlider!
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func sliderAction(_ sender: UISlider) {
        
        let valuse = Int(sender.value)
        let valueAlpha = CGFloat(sender.value/100)
        label.text = "Transparency: \(valuse)"
        appleOutlet.alpha = valueAlpha
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
  
}
